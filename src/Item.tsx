import { memo } from "react";

export const Itemcomponent = memo<{ selected: number; item: any }>(
  ({ selected, item }) => {
    return (
      <div className={selected === item.idx ? "item selected" : "item"}>
        Item {item.idx}
      </div>
    );
  }
);
