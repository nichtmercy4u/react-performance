const ROW_COUNT = 1000;
export const ITEM_COUNT = 10;

export const generateRows = () => {
  const data: any = [];

  for (let i = 0; i < ROW_COUNT; i++) {
    data[i] = [];
    for (let j = 0; j < ITEM_COUNT; j++) {
      const item = {
        y: i,
        x: j,
        idx: parseInt(`${i * ITEM_COUNT + j}`, 10),
      };
      data[i][j] = item;
    }
  }
  return data;
};
