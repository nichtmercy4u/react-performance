import { memo, useCallback, useEffect, useMemo, useRef, useState } from "react";
import "./app.css";
import { Itemcomponent } from "./Item";
import { RowComponent } from "./Row";
import { generateRows, ITEM_COUNT } from "./service";

const App = memo(() => {
  const data = useMemo(() => {
    return generateRows();
  }, []);

  const [selected, setSelected] = useState<number>(0);
  const scrollRef = useRef<HTMLDivElement>(null);

  const selectedRow = useMemo(() => {
    return Math.floor(selected / ITEM_COUNT);
  }, [selected]);

  useEffect(() => {
    if (scrollRef.current) {
      if (selectedRow) {
        const srcollIdx = selectedRow - 1;
        scrollRef.current.style.transform = `translateY(${-srcollIdx * 56}px)`;
      }
    }
  }, [selectedRow]);

  const onArrowPress = useCallback(
    (event) => {
      if (!event.repeat) {
        if (event.key === "ArrowDown") {
          if (selectedRow < data.length - 1) {
            setSelected((prev) => prev + data[selectedRow].length);
          }
        }
        if (event.key === "ArrowUp") {
          if (selectedRow) {
            setSelected((prev) => prev - data[selectedRow].length);
          }
        }
        if (event.key === "ArrowRight") {
          if (selected < data[selectedRow][data[selectedRow].length - 1].idx) {
            setSelected((prev) => prev + 1);
          }
        }
        if (event.key === "ArrowLeft") {
          if (selected > data[selectedRow][0].idx) {
            setSelected((prev) => prev - 1);
          }
        }
        // set first item
        if (event.key === "h" && scrollRef.current) {
          scrollRef.current.style.transform = `translateY(${0}px)`;
          setSelected(0);
        }
        // set last item
        if (event.key === "e") {
          setSelected(
            data[data.length - 1][data[data.length - 1].length - 1].idx
          );
        }
      }
    },
    [data, selectedRow, selected]
  );

  useEffect(() => {
    window.addEventListener("keydown", onArrowPress);

    return () => {
      window.removeEventListener("keydown", onArrowPress);
    };
  }, [onArrowPress]);

  const renderItemCallback = useCallback(
    (item) => {
      return (
        <Itemcomponent
          key={`item-${item.idx}`}
          selected={selected}
          item={item}
        />
      );
    },
    [selected]
  );

  const renderRowsCallback = useCallback(
    (row, idx: number) => {
      return (
        <RowComponent selectedRow={selectedRow} idx={idx} key={`row-${idx}`}>
          {row.map(renderItemCallback)}
        </RowComponent>
      );
    },
    [renderItemCallback, selectedRow]
  );

  return (
    <div className="container">
      <div className="header">
        <h1>Row rendering</h1>
        <p>
          Press up / down / left / right - arrows to navigate, h - first item, e
          - last item
        </p>
      </div>
      <div className="scroll-wrapper">
        <div ref={scrollRef} className="scroll-container">
          {data.map(renderRowsCallback)}
        </div>
      </div>
    </div>
  );
});

export default App;
