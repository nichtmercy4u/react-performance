import { memo } from "react";

export const RowComponent = memo<{
  selectedRow: number;
  idx: number;
  children?: any;
}>(({ selectedRow, idx, children }) => {
  return (
    <div
      className={selectedRow === idx ? "row selected" : "row"}
      key={`row-${idx}`}
    >
      {children}
    </div>
  );
});
